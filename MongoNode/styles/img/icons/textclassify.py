#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
# import libraries
import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile
 
df = pd.read_excel('Training Sheet.xlsx', sheetname='Training sheet')


print("Column headings:")
print(df.columns)
target=(df['Category'])

raw_train_data=df.loc[:, 'production_year':'movie_release_pattern_display_name']
print(raw_train_data.columns)
  
from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier(random_state=0)
clf.fit(raw_train_data,target)
df_test = pd.read_excel('Scoring Sheet.xlsx', sheetname='scoring-2012-challenge (1)')
data_test=df_test.loc[:, 'production_year':'movie_release_pattern_display_name']

clf.predict(data_test)
 

