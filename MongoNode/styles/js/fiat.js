// ----------------------------------------------------------------------------
// Controls the site-wide fiat conversion as per specified in menu bar
// ----------------------------------------------------------------------------
var oldFiat = 'USD';
var fiat = 'USD';
var toUSD = 1;

$(document).ready(function() {

  // Attach change handler to fiat dropdown in menu bar
  $('.ui.dropdown#fiat').dropdown({ onChange: onFiatChange });

});

// Obtain the exchange rate to apply to price fields --------------------------
function onFiatChange() {
  oldFiat = fiat;
  fiat = $('.ui.dropdown#fiat').dropdown('get value').toUpperCase();
  $('span#fiat').text(fiat);
  $.ajax({
    method: 'GET',
    url: '/api/convertFiat/' + oldFiat + '/' + fiat,
    dataType: 'json',
    success: function(json) {
      toUSD = json.usd;
      $.each($('.price'), function(index, value) {
        convertFiat($(value), json.conversion);
      });
      if ($('.priceChart').length > 0) convertChart(json.conversion);
    }
  });
}

// Intercept price changes to apply fiat exchange rate ------------------------
function getConversion(usd) {
  return formatPrice(parseValue(usd) * toUSD);
}

function convertFromUSD(field) {
  convertFiat(field, toUSD);
}

function convertFiat(field, conversion) {
  var html = field.html();
  var value = parseValue(html) * conversion;
  field.html(formatPrice(value));
}

function parseValue(value) {
  if (!value) return value;
  if (value.includes('.')) return value.replace(/[^0-9.]/g, '');
  else return value.replace(/\D/g, '');
}

function formatPrice(value) {
  if (value >= 10000) value = ('' + Math.round(value)).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  else if (value >= 1000) value = ('' + value).trim().substring(0, 1) + ',' + ('' + value.toFixed(2)).substring(1);
  else if (value > 1) value = value.toFixed(2);
  else value = value.toFixed(4);
  if (fiat == 'AUD') return 'AU$' + value;
  if (fiat == 'BRL') return 'R$' + value;
  if (fiat == 'CAD') return 'C$' + value;
  if (fiat == 'CHF') return 'Fr' + value;
  if (fiat == 'CNY') return 'Â¥' + value;
  if (fiat == 'CZK') return 'K' + value;
  if (fiat == 'DKK' || fiat == 'NOK' || fiat == 'SEK') return 'kr' + value;
  if (fiat == 'EUR') return 'â‚¬' + value;
  if (fiat == 'GBP') return 'Â£' + value;
  if (fiat == 'HKD') return 'HK$' + value;
  if (fiat == 'HUF') return 'Ft' + value;
  if (fiat == 'IDR') return 'Rp' + value;
  if (fiat == 'ILS') return 'â‚ª' + value;
  if (fiat == 'INR') return 'â‚¹' + value;
  if (fiat == 'JPY') return 'Â¥' + value;
  if (fiat == 'KRW') return 'â‚©' + value;
  if (fiat == 'MYR') return 'RM' + value;
  if (fiat == 'PHP') return 'â‚±' + value;
  if (fiat == 'PKR') return 'Rs' + value;
  if (fiat == 'PLN') return 'zÅ‚' + value;
  if (fiat == 'RUB') return 'â‚½' + value;
  if (fiat == 'SGD') return 'S$' + value;
  if (fiat == 'THB') return 'à¸¿' + value;
  if (fiat == 'TRY') return 'â‚º' + value;
  if (fiat == 'TWD') return 'NT$' + value;
  if (fiat == 'ZAR') return 'R' + value;
  if (fiat == 'EUR') return 'â‚¬' + value;
  return '$' + value;
}