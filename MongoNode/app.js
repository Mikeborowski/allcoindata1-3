// app.js 
var session = require('express-session');
var bodyParser = require('body-parser');
var express = require('express'); 
var http = require('http');
var https = require('https');
var fs = require('fs');
var config = require('config.json');
var options = {
    key:fs.readFileSync('silcKey/private-key.pem'),
    ca: fs.readFileSync('silcKey/gd_bundle-g2-g1.crt'),
    cert: fs.readFileSync('silcKey/4f1303ddfe75f567.crt'),
    //    ca:[fs.readFileSync(config.sslCaPath)]
};

function requireHTTPS(req, res, next) {

    if (!req.secure&& req.get('x-forwarded-proto') !== 'https') {
	//FYI this should work for local development as well
	return res.redirect('https://' + req.get('host') + req.url);
    }
    next();
}

var app = express(); 
var port =80; 
 
var itemRouter = require('./src/routes/itemRoutes'); 
 
app.set('view engine', 'ejs'); 
app.use(express.static('public')); 
app.use('/items', itemRouter); 
app.use('/styles',express.static(__dirname + "/styles")); 
 
app.use(requireHTTPS); 
app.use(bodyParser.urlencoded({ 
        extended: false 
})); 
app.use(bodyParser.json()); 
 
 
 
 

 
app.get('/', function (req, res) { 
  res.render('index'); 
}); 
 
app.get('/exchanges', function (req, res) { 
  res.render('exchanges'); 
}); 
 
app.get('/news', function (req, res) { 
  res.render('news'); 
}); 
 
app.get('/social', function (req, res) { 
  res.render('social'); 
}); 
 
app.get('/eventcalander', function (req, res) { 
  res.render('eventcalander'); 
}); 
 
app.get('/forums', function (req, res) { 
  res.render('forums'); 
}); 

app.get('/updates', function (req, res) {
  res.render('updates');
});

http.createServer(app).listen(80); 
https.createServer(options,app).listen(443); 
 

