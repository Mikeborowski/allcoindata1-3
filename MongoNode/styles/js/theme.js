var darkStyle = darkStyle = $('link[rel="stylesheet"]#darkTheme')[0];

console.log($.cookie('darktheme'));
if ($.cookie('darktheme') == 1)
  $(darkStyle).removeAttr('disabled');

// Default to light theme (Firefox workaround)
if ($(darkStyle).attr('disabled') == 'disabled')
  $(darkStyle).prop('disabled', 'disabled');


$(document).ready(function() {

  // Attach click handler to Lights button
  $('a.item#theme').click(function() {
    setGreen();
    setStyle();
    setChartVolColor();
  });

  // Toggle the dark theme stylesheet
  function setStyle() {
    if (lightsOn()) {
      $(darkStyle).removeAttr('disabled');
      $.cookie('darktheme', 1);
    } else {
      $(darkStyle).attr('disabled', 'disabled');
      $(darkStyle).prop('disabled', 'true');
      $.removeCookie('darktheme');
    }
  }

  // Modify the green text to contrast with the background
  function setGreen() {
    if (lightsOn()) {
      $('.colored').each(function(i) {
        if ($(this).css('color') == 'rgb(0, 128, 0)')
          $(this).css('color', 'rgb(154, 205, 50)');

      });
    } else {
      $('.colored').each(function(i) {
        if ($(this).css('color') == 'rgb(154, 205, 50)')
          $(this).css('color', 'rgb(0, 128, 0)');
      });
    }
  }

});

// Exposed Functionality ------------------------------------------------------
function lightsOn() {
  return $(darkStyle).attr('disabled') == 'disabled';
}

function getGreen() {
  return lightsOn() ? 'green' : 'yellowgreen';
}

function getChartVolColor() {
  return lightsOn() ? 'rgba(128, 128, 128, 0.4)' : 'rgba(0, 0, 0, 0.8)';
}

// Modify the color of the volume chart on the coin page
function setChartVolColor() {
  if (typeof(chart) != 'undefined' && chart) {
    chart.data.datasets[1].backgroundColor = getChartVolColor();
    chart.data.datasets[1].borderColor = getChartVolColor();
    chart.update();
  }
}